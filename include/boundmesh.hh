// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_BOUND_MESH_HH
#define FMM_BOUND_MESH_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// mlfmm headers
#include "multisources.hh"
#include "currentmesh.hh"
#include "currentsurface.hh"
#include "interp.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class BoundMesh> ShBoundMeshPr;
	typedef arma::field<ShBoundMeshPr> ShBoundMeshPrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class (still partially virtual)
	class BoundMesh: public MultiSources{
		// properties
		protected:
			// number of gauss points
			arma::sword num_gauss_volume_ = 1;
			arma::sword num_gauss_surface_ = 5;

			// pointers to source objects
			ShCurrentMeshPr current_mesh_;
			ShCurrentSurfacePr current_surface_;
			ShInterpPr interp_;

		// methods
		public:
			// constructors
			BoundMesh();
			BoundMesh(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &n,
				const arma::Mat<fltp> &Mn);

			// factory
			static ShBoundMeshPr create();
			static ShBoundMeshPr create(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &n,
				const arma::Mat<fltp> &Mn);

			// number of gauss points
			void set_num_gauss(
				const arma::sword num_gauss_volume, 
				const arma::sword num_gauss_surface);

			// set mesh
			void set_mesh(
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &n,
				const arma::Mat<fltp> &Mn);

	};

}}

#endif
