// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_NODELEVEL_HH
#define FMM_NODELEVEL_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <vector>
#include <thread> 
#include <mutex>
#include <limits>

// rat-common library headers
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"
#include "rat/common/log.hh"

// specific headers
#include "grid.hh"
#include "ilist.hh"
#include "stmat.hh"
#include "fmmmat.hh"
#include "settings.hh"

// CUDA specific includes
#ifdef ENABLE_CUDA_KERNELS
#include "gpukernels.hh"
#endif

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class NodeLevel> ShNodeLevelPr;

	// recursively builds tree structure
	class NodeLevel{
		// properties
		private:
			// other pointers
			ShGridPr grid_ = NULL;
			ShIListPr ilist_ = NULL;
			ShSettingsPr stngs_ = NULL;
			
			// location within tree
			ShNodeLevelPr next_level_ = NULL;
			NodeLevel* prev_level_ = NULL; // weak reference

			// counters
			arma::uword level_ = 0;
			arma::uword num_below_ = 0;
			arma::uword grid_dim_ = 0;

			// node information
			arma::uword num_nodes_;
			arma::Row<arma::uword> morton_index_;
			arma::Row<arma::uword> parent_;
			arma::Row<arma::uword> first_child_;
			arma::Row<arma::uword> last_child_;

			// indexing of multipoles
			arma::Row<arma::uword> multipole_index_; // storage location for multipoles
			arma::Row<arma::uword> localpole_index_; // storage location for localpoles

			// node coordinate
			arma::Mat<fltp> Rn_;
			arma::Mat<arma::uword> grid_location_;

			// number of sources and targets stored in children
			arma::uword num_source_nodes_;
			arma::uword num_target_nodes_;
			arma::Row<arma::uword> num_sources_;
			arma::Row<arma::uword> num_targets_;
			arma::Row<arma::uword> source_list_;
			arma::Row<arma::uword> target_list_;

			// direct interaction lists
			// for each target all sources
			arma::Row<arma::uword> num_direct_;
			arma::Row<arma::uword> num_direct_found_;
			arma::field<arma::Col<arma::uword> > direct_interactions_;
			
			// approximate interaction lists
			// for each target all sources
			arma::Row<arma::uword> num_approx_;
			arma::Row<arma::uword> num_approx_found_;
			arma::field<arma::Col<arma::uword> > approx_interactions_;
			arma::field<arma::Col<arma::uword> > approx_type_;

			// multipole to localpole list
			arma::Mat<arma::uword> fast_mp2lp_list_;
			arma::Row<arma::uword> fast_mp2lp_list_first_;
			arma::Row<arma::uword> fast_mp2lp_list_last_;
			
			// source to target lists (only used on leaf level)
			// *may want to consider making a subclass for leaf stuff only
			arma::field<arma::Col<arma::uword> > s2t_list_src_;
			arma::Col<arma::uword> s2t_list_tar_;

			// multipole and localpole data
			arma::Mat<std::complex<fltp> > Mp_;
			arma::Mat<std::complex<fltp> > Lp_;

			// thread handles for this level
			std::thread thread_mp2lp_;
			std::thread thread_so2ta_;
			
			// timings
			fltp last_structure_time_ = 0;
			fltp last_node_time_ = 0;
			fltp last_list_time_ = 0;
			fltp last_So2Ta_time_ = 0;
			fltp last_So2Mp_time_ = 0;
			fltp last_Mp2Lp_time_ = 0;
			fltp last_Mp2Mp_time_ = 0;
			fltp last_Lp2Lp_time_ = 0;
			fltp last_Lp2Ta_time_ = 0;

		// methods
		public:
			// constructors
			NodeLevel(); // default constructor
			NodeLevel(const ShSettingsPr &stngs, const ShIListPr &ilist, const ShGridPr &leaf); // with extra input
			explicit NodeLevel(NodeLevel* prev_level); // constructor from previous level
			
			// destructor
			~NodeLevel();

			// factory
			static ShNodeLevelPr create();
			static ShNodeLevelPr create(const ShSettingsPr &stngs, const ShIListPr &ilist, const ShGridPr &grid);
			static ShNodeLevelPr create(NodeLevel* prev_level);


			// settings object
			void set_settings(const ShSettingsPr &stng);

			// setting of pointers
			void set_grid(const ShGridPr &grid);
			void set_ilist(const ShIListPr &ilist);

			// tree creation
			void setup(const cmn::ShLogPr &lg);
			void setup_structure();
			arma::uword setup_structure(const arma::uword ilevel);
			void setup_node_locations();
			void setup_interaction_lists();
			void compact_tree();
			void setup_mp2lp_lists();

			// source to target lists (used by s2t_ab)
			void setup_s2t_lists();

			// setting indexes for multipoles and localpoles
			void setup_multipoles();

			// setting
			void set_parent(const arma::Mat<arma::uword> &parent_index);

			// get source and target info
			const arma::Row<arma::uword>& get_morton() const;
			const arma::Row<arma::uword>& get_num_sources() const;
			const arma::Row<arma::uword>& get_num_targets() const;
			const arma::Mat<arma::uword>& get_grid_location() const;
			const arma::Mat<fltp>& get_node_coords() const;
			const arma::Row<arma::uword>& get_first_child() const;
			const arma::Row<arma::uword>& get_last_child() const;
			const arma::Row<arma::uword>& get_multipole_index() const;
			
			// get source to target lists
			const arma::field<arma::Col<arma::uword> >& get_s2t_lists_src() const;
			const arma::Col<arma::uword>& get_s2t_list_tar() const;

			// get next level method
			ShNodeLevelPr get_next_level();

			// get counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_source_nodes() const;
			arma::uword get_num_target_nodes() const;
			arma::uword get_level() const;
			arma::uword get_num_below() const;
			arma::uword get_mp2lp_list_size() const;

			// method for getting source to target list
			// arma::field<arma::Row<arma::uword> > get_s2t_list() const;

			// approximation lists
			//arma::Mat<arma::uword> source_to_multipole_list() const;
			//arma::Mat<arma::uword> localpole_to_target_list() const;

			// get leaf
			const ShNodeLevelPr& get_leaf()const;

			// fast multipole method steps
			void run_mlfmm(const cmn::ShLogPr &lg); 
			void mlfmm_upward_pass(const cmn::ShLogPr &lg);
			void mlfmm_downward_pass(const cmn::ShLogPr &lg);

			// allocation
			void allocate_multipoles();
			void deallocate_multipoles();
			void allocate_localpoles();
			void deallocate_localpoles();
			void deallocate_recursively();

			// MLFMM steps
			void source_to_multipole(); 
			void multipole_to_multipole();
			void multipole_to_localpole();
			void multipole_to_localpole_by_type();
			void multipole_to_localpole_by_target();
			void localpole_to_localpole();
			void localpole_to_target();
			void source_to_target();

			#ifdef ENABLE_CUDA_KERNELS
			// MLFMM steps using cuda
			void mp2lp_batch_sorting();
			void multipole_to_localpole_cuda();
			#endif
			
			// check location in tree
			bool is_leaf() const;
			bool is_root() const;

			// display function
			void display(const cmn::ShLogPr &lg) const;
	};

}}

#endif
