// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_SPHARM_HH
#define FMM_SPHARM_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

// mlfmm headers
#include "extra.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// harmonics class template
	class Spharm{
		// properties
		private:
			int num_exp_;
			int num_harm_;
			arma::Mat<std::complex<fltp> > Y_;

		// methods
		public:
			// regular constructor
			explicit Spharm(const int num_exp);
			Spharm(const int num_exp, const arma::Mat<fltp> &sphcoord); // calculate regular harmonic

			// different versions of the spherical harmonic expansion
			void calculate(const arma::Mat<fltp> &sphcoord);

			// setting and getting
			arma::Row<std::complex<fltp> > get_nm(const int n, const int m) const; // get row
			std::complex<fltp> get_nm(const int i, const int n, const int m) const; // get single value
			const arma::Mat<std::complex<fltp> >& get_all() const;

			// operators (used for calculating finite difference)
			Spharm operator-(const Spharm &otherharmonic);
			Spharm operator/(const fltp &div);
			void divide(const arma::Row<fltp> &div);

			// displaying
			void display(const cmn::ShLogPr &lg) const; // display stored harmonic in convenient format
			void display(const int i, const cmn::ShLogPr &lg) const; // display stored harmonic in convenient format
	};

}}

#endif