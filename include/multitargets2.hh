// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MULTI_TARGETS2_HH
#define FMM_MULTI_TARGETS2_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>

// mlfmm headers
#include "mgntargets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MultiTargets2> ShMultiTargets2Pr;
	typedef arma::field<ShMultiTargets2Pr> ShMultiTargets2PrList;

	// This object combines sources such that the
	// MLFMM can treat them as one object
	// this comes at the cost of overhead
	// but at least it is optional
	class MultiTargets2: public MgnTargets{
		// properties
		private:
			// list of targets
			ShTargetsPrList tars_;

			// target index
			arma::Row<arma::uword> target_index_;

		// methods
		public:
			// constructor
			MultiTargets2();
			explicit MultiTargets2(const ShTargetsPrList& targets); 

			// factory
			static ShMultiTargets2Pr create();
			static ShMultiTargets2Pr create(const ShTargetsPrList& targets);
			
			// factory with source list input
			template<typename T> static ShMultiTargets2Pr create(arma::field<std::shared_ptr<T> > &tar_list);

			// get the number of target objects
			arma::uword get_num_target_objects() const;

			// setup function
			void setup_targets() override;

			// post processing
			void post_process() override;

			// add targets
			void add_targets(const ShTargetsPr& targets);
			template<typename T> void add_targets(arma::field<std::shared_ptr<T> > &tar_list);

			// allocate
			void allocate() override;
	};


	// special factory method
	template<typename T> ShMultiTargets2Pr MultiTargets2::create(
		arma::field<std::shared_ptr<T> > &tar_list){
		ShMultiTargets2Pr multi = MultiTargets2::create();
		multi->add_targets(tar_list);
		return multi;
	}

	// add a list of targets
	template<typename T> void MultiTargets2::add_targets(
		arma::field<std::shared_ptr<T> > &tar_list){
		for(arma::uword i=0;i<tar_list.n_elem;i++)add_targets(tar_list(i));
	}

}}

#endif