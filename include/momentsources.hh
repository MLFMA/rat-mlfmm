// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MOMENT_SOURCES_HH
#define FMM_MOMENT_SOURCES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm headers
#include "settings.hh"
#include "sources.hh"
#include "savart.hh"
#include "stmat.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MomentSources> ShMomentSourcesPr;

	// collection of line sources
	// is derived from the sources class (still mostly virtual)
	class MomentSources: virtual public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// coordinates in [m]
			arma::Mat<fltp> Rs_;

			// magnetic moments in [Am]
			arma::Mat<fltp> Mm_;

			// number of sources
			arma::uword num_sources_ = 0;

			// source to multipole matrices
			StMat_So2Mp_J M_M_;
			arma::Mat<fltp> dR_;

		// methods
		public:
			// constructor
			MomentSources();
			MomentSources(const arma::Mat<fltp> &Rs, const arma::Mat<fltp> &Mm);

			// factory
			static ShMomentSourcesPr create();
			static ShMomentSourcesPr create(const arma::Mat<fltp> &Rs, const arma::Mat<fltp> &Mm);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<fltp> &Rs);

			// set direction vector
			void set_moments(const arma::Mat<fltp> &Mm);

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const ShSettingsPr &stngs) const override;
			
			// direct field calculation for both A and B
			void calc_direct(
				const ShTargetsPr &tar, 
				const ShSettingsPr &stngs) const override;
			void source_to_target(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const override;
			
			// shape for testing
			void setup_ring_magnet(
				const fltp Rin, 
				const fltp Rout, 
				const fltp height, 
				const arma::uword nr, 
				const arma::uword nz, 
				const arma::uword nl, 
				const fltp Max, 
				const fltp Mrad);
	};

}}

#endif
