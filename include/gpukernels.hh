// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_GPU_KERNELS_HH
#define FMM_GPU_KERNELS_HH

// general headers
#include <complex>
#include <cmath>
#include <cassert>
#include <set>
#include <list>
#include <vector>
#include <algorithm>
#include <future>
#include <thread>
#include <atomic>

// specific headers
#include "rat/common/log.hh"

#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// this class contains the gpu kernels
	class GpuKernels{
		// methods
		public:
			// get information on available devices
			static int get_num_devices();
			static void show_device_info(const std::set<int> &gpu_devices, const cmn::ShLogPr &lg = cmn::NullLog::create());
			static void show_device_info(const int device_index, const cmn::ShLogPr &lg = cmn::NullLog::create());
			static std::string get_device_name(const int device_index);

			// set gpu device for the active thread
			static void set_device(const int device_index);
			static int get_device();

			// source to multipole kernel
			static void so2mp_kernel(
				std::complex<cufltp> *Mp_ptr,
				const int num_exp,
				const cufltp *Rn,
				const cufltp *dR,
				const long long unsigned int num_sources,
				const long long unsigned int *first_source,
				const long long unsigned int *last_source,
				const long long unsigned int num_nodes,
				const std::set<int> &gpu_devices,
				const bool is_managed = false);

			// multipole to localpole kernel
			static void mp2lp_kernel(
				const long long unsigned int num_dim, // number of dimensions
				const int polesize, // size of the multipoles and localpoles
				std::complex<cufltp> *Lp_ptr, // matrix with target localpoles
				const long long unsigned int num_lp, // number of localpoles
				const std::complex<cufltp> *Mp_ptr, // matrix with target multipoles
				const long long unsigned int num_mp, // number of multipoles
				const std::complex<cufltp> *Mint_ptr, // matrix with transformation matrices
				const long long unsigned int num_int_type, // number of interaction types
				const long long unsigned int *type_ptr, // interaction type
				const long long unsigned int *source_node, // target multipoles
				const long long unsigned int *target_node, // target localpoles
				const long long unsigned int *mp_idx, // source multipoles
				const long long unsigned int *lp_idx,
				const long long unsigned int *first_index,
				const long long unsigned int *last_index,
				const long long unsigned int num_group,
				const std::set<int> &gpu_devices);
			
			// source to target kernel
			static void so2ta_kernel(
				cufltp *A_ptr, 
				cufltp *H_ptr, 
				const cufltp *Rt_ptr,
				const long unsigned int num_targets,
				const cufltp *Rs_ptr, 
				const cufltp *Ieff_ptr, 
				const long unsigned int num_sources,
				const long unsigned int* first_target_ptr,			
				const long unsigned int* last_target_ptr,
				const long unsigned int num_target_nodes,
				const long unsigned int* first_source_ptr,			
				const long unsigned int* last_source_ptr,
				const long unsigned int num_source_nodes,
				const long unsigned int* target_list_ptr,
				const long unsigned int* source_list_idx_ptr,
				const long unsigned int num_target_list,
				const long unsigned int* source_list_ptr,
				const long unsigned int num_source_list,
				const bool calc_A, const bool calc_H, const bool vl,
				const std::set<int> &gpu_devices,
				const bool is_managed = false);

			// direct source to target kernel
			static void direct_kernel(
				cufltp *A_ptr, 
				cufltp *H_ptr, 
				const cufltp *Rt_ptr,
				const long unsigned int num_targets,
				const cufltp *Rs_ptr, 
				const cufltp *Ieff_ptr, 
				const long unsigned int num_sources,
				const bool calc_A, const bool calc_H, const bool vl,
				const std::set<int> &gpu_devices, const bool is_managed = false);

			static void test_kernel(
				cufltp* A, 
				cufltp *B, 
				unsigned int nn);

			// create host pinned memory
			static void create_pinned(
				void ** ptr, 
				size_t size);

			// free host pinned memory
			static void free_pinned(
				void* ptr);

			// create managed memory
			static void create_cuda_managed(
				void** ptr, 
				size_t size);

			// memory destruction
			static void free_cuda_managed(
				void* ptr);

			// copy data to GPU
			static void data2gpu(void** ptr, void* data, size_t size);

			// free data copied to gpu
			static void free_gpu(void* ptr);

			// sorting
			static void sort(unsigned int *data, int num_items);


	};

}}

#endif