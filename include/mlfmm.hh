// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MLFMM_HH
#define FMM_MLFMM_HH

// general headers
#include <armadillo> 
#include <cmath>
#include <cassert>
#include <memory>
#include <thread>

// specific headers
#include "rat/common/error.hh"
#include "settings.hh"
#include "ilist.hh"
#include "grid.hh"
#include "nodelevel.hh"
#include "sources.hh"
#include "targets.hh"
#include "background.hh"
#include "rat/common/log.hh"

// CUDA specific includes
#ifdef ENABLE_CUDA_KERNELS
#include "gpukernels.hh"
#endif

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Mlfmm> ShMlfmmPr;

	// Multipole Method Controler class
	// this is a wrapper class to simplify 
	// the use of the fast multipole method
	class Mlfmm{
		private:
			// do not allocate
			bool no_allocate_ = false;

			// sources and targets
			ShSourcesPr src_ = NULL;
			ShTargetsPr tar_ = NULL;

			// background field
			ShBackgroundPr bg_ = NULL;

			// input settings
			ShSettingsPr stngs_ = NULL;

			// interaction list
			ShIListPr ilist_ = NULL;

			// computation grid
			ShGridPr grid_ = NULL;

			// octtree root
			ShNodeLevelPr root_ = NULL;

			// timers
			fltp setup_time_ = 0;
			fltp last_calculation_time_ = 0;
			fltp life_calculation_time_ = 0;

		// methods
		public:
			// constructors
			Mlfmm();
			Mlfmm(const ShSourcesPr &src, const ShTargetsPr &tar);
			Mlfmm(const ShSourcesPr &src, const ShTargetsPr &tar, const ShSettingsPr &stngs);
			explicit Mlfmm(const ShSettingsPr &stngs);

			// factory
			static ShMlfmmPr create();
			static ShMlfmmPr create(const ShSourcesPr &src, const ShTargetsPr &tar);
			static ShMlfmmPr create(const ShTargetsPr &tar, const ShSourcesPr &src);
			static ShMlfmmPr create(const ShTargetsPr &tar, const ShSourcesPr &src, const ShSettingsPr &stngs);
			static ShMlfmmPr create(const ShSourcesPr &src, const ShTargetsPr &tar, const ShSettingsPr &stngs);
			static ShMlfmmPr create(const ShSettingsPr &stngs);

			// set allocation flag
			void set_no_allocate(const bool no_allocate = true);

			// set pointers
			void set_sources(const ShSourcesPr &src);
			void set_targets(const ShTargetsPr &tar);
			void set_background(const ShBackgroundPr &bg);

			// set settings externally
			void set_settings(const ShSettingsPr &settings);

			// grid and tree setup
			void setup(const cmn::ShLogPr &lg = cmn::NullLog::create());

			// main calculation
			void calculate(const cmn::ShLogPr &lg = cmn::NullLog::create());

			// direct calculation
			void calculate_direct(const cmn::ShLogPr &lg = cmn::NullLog::create());

			// display function
			void display(const cmn::ShLogPr &lg) const;
			
			// access settings
			ShSettingsPr& settings();

			// get calculation time
			void add_calculation_time(const fltp time);
			fltp get_setup_time() const;
			fltp get_last_calculation_time() const;
			fltp get_life_calculation_time() const;

			// check whether to use direct mode
			bool use_direct() const;

			// access stored objects
			const ShNodeLevelPr& get_root()const;
			const ShGridPr& get_grid()const;
	};

}}

#endif