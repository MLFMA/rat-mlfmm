// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Description:
/* Soleno rewrite to C++ file by Jeroen van Nugteren
note that this was done by direct translation
without knowledge on the inner workings of the code
Uses parallel computing to attain maximum performance on multicore systems

Soleno was originally developed at Twente University 
by Gert Mulder, and upgraded in 1999 by Erik Krooshoop

This function calculates the field at (R,Z) using:
[Br,Bz] = soleno(R,Z,Rin,Rout,Zmin,Zmax,I,Nlayers)

Rin,Rout,Zmin,Zmax,I and Nlayers define the coils and must be equal in size
R and Z define the position and must be equal in size as well
output Br and Bz is respectively the field in the radial and axial direction*/

// include guard
#ifndef FMM_SOLENO_HH
#define FMM_SOLENO_HH

// general headers
#include <cmath>
#include <armadillo>
#include <cassert>
#include <memory>

// common headers
#include "rat/common/parfor.hh"

// mlfmm headers
#include "typedefs.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Soleno> ShSolenoPr;

	// Soleno class
	class Soleno{
		// private
		public:
			// parallel computation
			bool use_parallel_ = true;

			// inner and outer radius
			arma::Row<fltp> Rin_;
			arma::Row<fltp> Rout_; 

			// coil height
			arma::Row<fltp> zlow_;
			arma::Row<fltp> zhigh_;

			// current
			arma::Row<fltp> current_;
			arma::Row<fltp> num_turns_;

			// keep track of number of solenoids
			arma::uword num_solenoids_ = 0;

			// number of layers
			arma::Row<arma::uword> num_layers_;

		// original soleno functions
		// written in C so this looks a bit meh
		private:
			// magnetic field calculation
			static int sign(fltp a);
			static void rekenb(fltp *Brh, fltp *Bzh, const fltp Zmh, const fltp Rm, const fltp Rp);
			static void calc_B_original(fltp *Brt, fltp *Bzt, const fltp R, const fltp Z, const fltp Rin, const fltp Rout, const fltp Zlow, const fltp Zhigh, const fltp It, const arma::uword Nlayers);
			static void calc_B_extended(fltp *Brt, fltp *Bzt, const fltp R, const fltp Z, const fltp Rin, const fltp Rout, const fltp Zlow, const fltp Zhigh, const fltp I, const arma::uword Nlayers);
			
			// inductance calculation
			static fltp Sol_CI(const fltp R1, const fltp R2, const fltp ZZ);
			static fltp CalcMutSub(const fltp zlow1, const fltp zlow2, const fltp zhigh1, const fltp zhigh2, const fltp Rin1, const fltp Rin2, const fltp Rout1, const fltp Rout2, const fltp Nw1, const fltp Nw2, const arma::uword NL1, const arma::uword NL2);
		
		// methods
		public:
			// constructor
			Soleno();

			// factory
			static ShSolenoPr create();

			// geometry setup
			void set_solenoid(const fltp Rin, const fltp Rout, const fltp zlow, const fltp zhigh, const fltp current, const fltp num_turns, const arma::uword num_layers = 5);
			void set_solenoid(const arma::Row<fltp> &Rin, const arma::Row<fltp> &Rout, const arma::Row<fltp> &zlow, const arma::Row<fltp> &zhigh, const arma::Row<fltp> &current, const arma::Row<fltp> &num_turns, const arma::Row<arma::uword> &num_layers); 
			void add_solenoid(const fltp Rin, const fltp Rout, const fltp zlow, const fltp zhigh, const fltp current, const fltp num_turns, const arma::uword num_layers = 5);
			void add_solenoid(const arma::Row<fltp> &Rin, const arma::Row<fltp> &Rout, const arma::Row<fltp> &zlow, const arma::Row<fltp> &zhigh, const arma::Row<fltp> &current, const arma::Row<fltp> &num_turns, const arma::Row<arma::uword> &num_layers); 

			// calculate field at target points
			arma::Mat<fltp> calc_B(const arma::Mat<fltp> &Rt) const;

			// calculate inductance matrix
			arma::Mat<fltp> calc_M() const;

			// calculate field on mlfmm targets
			void calc_field(const ShTargetsPr &tar);
	};

}}

#endif