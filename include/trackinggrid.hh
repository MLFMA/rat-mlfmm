// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_TRACKING_HH
#define FMM_TRACKING_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>
#include <map>

// mlfmm headers
#include "grid.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class TrackingGrid> ShTrackingGridPr;

	// harmonics class template
	class TrackingGrid: public Grid{
		// properties
		protected:
			// localpoles
			arma::Mat<std::complex<fltp> > Lp_;

			// source to target data
			std::map<arma::uword, arma::uword> node2stlist_;
			arma::Col<arma::uword> target_list_;
			arma::field<arma::Col<arma::uword> > source_list_;

			// conversion of morton index to node index
			std::map<arma::uword, arma::uword> morton2node_;

		// methods
		public:
			// constructor
			TrackingGrid();
			static ShTrackingGridPr create();

			// check if we are on grid
			arma::Row<arma::uword> is_inside(const arma::Mat<fltp> &Rt)const;

			// setup function
			virtual void setup(const cmn::ShLogPr &lg = rat::cmn::NullLog::create()) override; 
			void setup_direct();

			// override localpole to target
			void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp) override;
			void source_to_target(
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list) override;

			// calculate field at target points
			void calc_field(const ShTargetsPr &tar);


	};

}}

#endif