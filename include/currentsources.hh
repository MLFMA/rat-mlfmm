// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_CURRENT_SOURCES_HH
#define FMM_CURRENT_SOURCES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "stmat.hh"
#include "targets.hh"
#include "settings.hh"

// CUDA specific headers
#ifdef ENABLE_CUDA_KERNELS
#include "gpukernels.hh"
#endif

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class CurrentSources> ShCurrentSourcesPr;
	typedef arma::field<ShCurrentSourcesPr> ShCurrentSourcesPrList;

	// collection of line sources
	// is derived from the sources class
	class CurrentSources: virtual public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// coordinates in [m]
			arma::Mat<fltp> Rs_;

			// direction vector in [m]
			arma::Mat<fltp> dRs_;

			// current [A]
			arma::Row<fltp> Is_;

			// softening factor [m]
			arma::Row<fltp> epss_;

			// number of sources
			arma::uword num_sources_ = 0;

			// van Lanen kernel for vector potential calculation
			// this kernel approximates the elements as line currents
			// instead as points and results in a more accurate vector
			// potential and magnetic field when the elements are 
			// close to one another
			bool use_van_Lanen_ = true;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<fltp> dR_;

		// methods
		public:
			// constructor
			CurrentSources();
			CurrentSources(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &epss);
			explicit CurrentSources(
				const ShCurrentSourcesPrList &csl);

			// factory
			static ShCurrentSourcesPr create();
			static ShCurrentSourcesPr create(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &epss);
			static ShCurrentSourcesPr create(
				const ShCurrentSourcesPrList &csl);

			// set sources by combining other source objects
			void set_sources(
				const ShCurrentSourcesPrList &csl);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<fltp> &Rs);

			// setting and getting direction vector
			void set_direction(const arma::Mat<fltp> &dRs);
			arma::Mat<fltp> get_direction() const;

			// setting of the softening factor
			void set_softening(const arma::Row<fltp> &epss);

			// set currents
			void set_currents(const arma::Row<fltp> &Is);

			// set van Lanen kernel
			void set_van_Lanen(const bool use_van_Lanen = true);
			bool get_van_Lanen() const;

			// get element size
			fltp element_size() const override;

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;
			arma::Mat<fltp> get_source_direction() const;
			arma::Row<fltp> get_source_currents() const;
			arma::Row<fltp> get_source_softening() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const ShSettingsPr &stngs) const override;

			// direct field calculation for both A and B
			void calc_direct(
				const ShTargetsPr &tar, 
				const ShSettingsPr &stngs) const override;
			void source_to_target(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const override;

			// set elements for solenoid
			void translate(
				const fltp dx, 
				const fltp dy, 
				const fltp dz);
			void setup_solenoid(
				const fltp Rin, 
				const fltp Rout, 
				const fltp height, 
				const arma::uword nr, 
				const arma::uword nz, 
				const arma::uword nl, 
				const fltp J);

			// gpu version of source to target, source to multipole and direct kernels
			#ifdef ENABLE_CUDA_KERNELS
			void calc_direct_gpu(
				const ShTargetsPr &tar, 
				const ShSettingsPr &stngs) const;
			void source_to_target_gpu(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const;
			void source_to_target_gpu_alt(
				const ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const ShSettingsPr &stngs) const;
			void source_to_multipole_gpu(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const ShSettingsPr &stngs) const;
			#endif
	};

}}

#endif
