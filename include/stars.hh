// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_STARS_HH
#define FMM_STARS_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"

// mlfmm headers
#include "sources.hh"
#include "targetpoints.hh"
#include "stmat.hh"
#include "settings.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Stars> ShStarsPr;

	// collection of masses in stellar dynamics
	// is derived from the sources class
	class Stars: virtual public Sources, public TargetPoints{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 1;

			// coordinates in [m]
			arma::Mat<fltp> Rs_;

			// number of solar masses
			arma::Mat<fltp> Ms_;

			// softening factor [m]
			arma::Row<fltp> epss_;

			// number of sources
			arma::uword num_sources_ = 0;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<fltp> dR_;

			// multipole operation matrices
			StMat_Lp2Ta M_A_; // for vector potential
			StMat_Lp2Ta_Grad M_F_;
			arma::Mat<fltp> dRl2p_;

		// methods
		public:
			// constructor
			Stars();
			Stars(const arma::Mat<fltp> &Rs, const arma::Row<fltp> &Ms, const arma::Row<fltp> &epss);

			// factory
			static ShStarsPr create();
			static ShStarsPr create(const arma::Mat<fltp> &Rs, const arma::Row<fltp> &Ms, const arma::Row<fltp> &epss);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<fltp> &Rs);

			// set solar masses
			void set_mass(const arma::Row<fltp> &Ms);

			// get mass
			arma::Mat<fltp> get_mass() const;

			// setting of the softening factor
			void set_softening(const arma::Row<fltp> &epss);

			// set memory efficiency
			void set_memory_efficent_s2m(const bool enable_memory_efficient_s2m);
			
			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<fltp> &dR, const ShSettingsPr &stngs) override;
			void source_to_multipole(arma::Mat<std::complex<fltp> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const ShSettingsPr &stngs) const override;
			
			// direct field calculation for both A and B
			void calc_direct(const ShTargetsPr &tar, const ShSettingsPr &stngs) const override;
			void source_to_target(const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const override;

			// localpole to target virtual functions
			void setup_localpole_to_target(const arma::Mat<fltp> &dR, const arma::uword num_dim, const ShSettingsPr &stngs) override;
			void localpole_to_target(const arma::Mat<std::complex<fltp> > &Lp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::uword num_dim, const ShSettingsPr &stngs) override;

			// direct calculation of gravity 
			static arma::Row<fltp> calc_M2V_s(const arma::Mat<fltp> &Rs, const arma::Row<fltp> &Ms, const arma::Row<fltp> &epss,const arma::Mat<fltp> &Rt, const bool use_parallel);
			static arma::Mat<fltp> calc_M2F_s(const arma::Mat<fltp> &Rs, const arma::Row<fltp> &Ms, const arma::Row<fltp> &epss,const arma::Mat<fltp> &Rt, const bool use_parallel);
	};

}}

#endif
