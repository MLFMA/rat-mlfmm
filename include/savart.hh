// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_SAVART_HH
#define FMM_SAVART_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>

#include "rat/common/typedefs.hh"

// contains direct biot-savart calculations

// code specific to Rat
namespace rat{namespace fmm{

	// Biot-Savart class
	class Savart{
		// methods
		public:
			// direct field calculation from currents without softening factor
			static arma::Mat<fltp> calc_I2A(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Ieff, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
			static arma::Mat<fltp> calc_I2H(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Ieff, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);

			// direct field calculation from magnetic moments without softening factor
			static arma::Mat<fltp> calc_M2A(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Meff, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
			static arma::Mat<fltp> calc_M2H(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Meff, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);

			// direct field calculation from currents with softening factor
			static arma::Mat<fltp> calc_I2A_s(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Ieff, 
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
			static arma::Mat<fltp> calc_I2H_s(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &Ieff,
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);

			// biot savart with line element approximation
			static arma::Mat<fltp> calc_I2H_vl(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
			static arma::Mat<fltp> calc_I2H_vl_dir(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);

			// van Lanen kernel more accurate version for calculating 
			// the vector potential of line current elements
			static arma::Mat<fltp> calc_I2A_vl(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
			static arma::Mat<fltp> calc_I2A_vl_dir(
				const arma::Mat<fltp> &Rs, 
				const arma::Mat<fltp> &dRs, 
				const arma::Row<fltp> &Is, 
				const arma::Row<fltp> &eps, 
				const arma::Mat<fltp> &Rt, 
				const bool use_parallel);
	};

}}

#endif