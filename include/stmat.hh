// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// This file contains the matrices for forming multipoles from sources and calculating field at targets

// include guard
#ifndef FMM_STMAT_HH
#define FMM_STMAT_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <iostream>
#include <memory>

// common headers
#include "rat/common/extra.hh"

// specific headers
#include "extra.hh"
#include "spharm.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition for source to multipole
	typedef std::shared_ptr<class StMat_So2Mp_J> ShStMat_So2Mp_JPr;
	typedef std::shared_ptr<class StMat_So2Mp_M> ShStMat_So2Mp_MPr;

	// shared pointer definition for multipole to target
	typedef std::shared_ptr<class StMat_Mp2Ta> ShStMat_Mp2TaPr;

	// shared pointers for localpole to target
	typedef std::shared_ptr<class StMat_Lp2Ta> ShStMat_Lp2TaPr;
	typedef std::shared_ptr<class StMat_Lp2Ta_Curl> ShStMat_Lp2Ta_CurlPr;
	typedef std::shared_ptr<class StMat_Lp2Ta_Grad> ShStMat_Lp2Ta_GradPr;

	// source to multipole
	class StMat_So2Mp_J{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_sources_; // number of sources
			arma::Mat<std::complex<fltp> > M_;

		// methods
		public:
			// constructor
			StMat_So2Mp_J();
			StMat_So2Mp_J(const int num_exp, const arma::Mat<fltp> &dR);	
			
			// factory
			static ShStMat_So2Mp_JPr create();

			// setting of number of expansions
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position
			void calc_matrix(const arma::Mat<fltp> &dR);

			// applying of the matrix
			arma::Mat<std::complex<fltp> > apply(const arma::Mat<fltp> &Ieff) const;
			arma::Mat<std::complex<fltp> > apply(const arma::Mat<fltp> &Ieff, const arma::Mat<arma::uword> &indices) const;
			arma::Mat<std::complex<fltp> > apply(const arma::Mat<fltp> &Ieff, arma::uword first_index, arma::uword last_index) const;

			// check methods
			bool is_empty() const;

			// display methods
			void display() const; // display matrix values in terminal
			//void display(const int k) const; // display matrix values in terminal
	};

	// source to multipole
	class StMat_So2Mp_M{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_sources_; // number of sources
			arma::Mat<std::complex<fltp> > M_;
			bool use_parallel_ = true;
			arma::Mat<std::complex<fltp> >::fixed<9,3> Mconv_;

		// methods
		public:
			// constructor
			StMat_So2Mp_M();
			StMat_So2Mp_M(const int num_exp, const arma::Mat<fltp> &dR);

			// factory
			static ShStMat_So2Mp_MPr create();

			// setting of number of expansions	
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position
			void calc_matrix(const arma::Mat<fltp> &dR);

			// applying of the matrix
			arma::Mat<std::complex<fltp> > apply(const arma::Mat<fltp> &Meff) const;
			arma::Mat<std::complex<fltp> > apply(const arma::Mat<fltp> &Meff, const arma::Mat<arma::uword> &indices) const;
			
			// check methods
			bool is_empty() const;
			
			// display methods
			void display() const; // display matrix values in terminal
			//void display(const int k) const; // display matrix values in terminal

			// other methods
			static arma::Mat<std::complex<fltp> >::fixed<9,3> get_mini_multipole_matrix();
			void set_M(const arma::uword num_sources, const arma::Mat<std::complex<fltp> > &M, const int num_exp);
			arma::Mat<std::complex<fltp> > get_M() const;
			arma::Mat<std::complex<fltp> > get_matrix(const arma::uword k) const;

			// enabling parallel calculations
			void set_use_parallel(const bool use_parallel);
	};

	// multipole to target
	class StMat_Mp2Ta{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_targets_; // number of target points
			arma::Mat<std::complex<fltp> > M_;

		// methods
		public:
			// constructor
			StMat_Mp2Ta();
			StMat_Mp2Ta(const int num_exp, const arma::Mat<fltp> &dR);

			// factory
			static ShStMat_Mp2TaPr create();

			// setting of number of expansions	
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position
			void calc_matrix(const arma::Mat<fltp> &dR);

			// applying of the matrix
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Mp) const;

			// applying of the matrix
			bool is_empty() const;

			// display methods
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal
	};

	// localpole to target for vector potential
	// calculates scalar/vector potential of any number of dimensions
	class StMat_Lp2Ta{
		// properties
		private:
			int num_exp_ = 0; // number of expansions
			arma::uword num_targets_; // number of target points
			arma::Mat<std::complex<fltp> > M_;

		// methods
		public:
			// constructor
			StMat_Lp2Ta();
			StMat_Lp2Ta(const int num_exp, const arma::Mat<fltp> &dR);

			// factory
			static ShStMat_Lp2TaPr create();

			// setting of number of expansions
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position	
			void calc_matrix(const arma::Mat<fltp> &dR);

			// applying of the matrix
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp) const;
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp, const arma::Mat<arma::uword> &indices) const;
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp, const arma::uword first_target, const arma::uword last_target) const;
			
			// applying of the matrix
			bool is_empty() const;

			// display methods
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal
	};

	// localpole to target for magnetic field
	// calculates curl of 3 dimensional potential
	class StMat_Lp2Ta_Curl{
		// properties
		private:
			// number of expansions
			int num_exp_ = 0; 

			// number of target points
			arma::uword num_targets_; 

			// the matrix
			arma::Mat<std::complex<fltp> > M_;

			// use parallel during computing the matrix
			// bool use_parallel_ = true;

			// mini matrix
			arma::Mat<std::complex<fltp> >::fixed<3,9> Mconv_;
			// arma::SpMat<std::complex<fltp> > Mconv_;

		// methods
		public:
			// constructor
			StMat_Lp2Ta_Curl();
			StMat_Lp2Ta_Curl(const int num_exp, const arma::Mat<fltp> &dR);	

			// factory
			static ShStMat_Lp2Ta_CurlPr create();

			// setting of number of expansions
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position
			void calc_matrix(const arma::Mat<fltp> &dR);
			// void calc_matrix2(const arma::Mat<fltp> &dR);
			arma::Mat<std::complex<fltp> > get_matrix() const;
			arma::Mat<std::complex<fltp> > get_matrix(const arma::uword k) const;
			
			// applying of the matrix
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp) const;
			// arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp, const arma::Mat<arma::uword> &target_index) const;
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp, const arma::uword first_index, const arma::uword last_index) const;

			// arma::Mat<fltp> apply_helper(const arma::Mat<fltp> &dLLp, const arma::uword N) const;
			// static arma::Mat<fltp>::fixed<3,9> get_cross_product_matrix();
			static arma::Mat<std::complex<fltp> >::fixed<9,3> get_mini_multipole_matrix();

			// applying of the matrix
			bool is_empty() const;
			
			// display methods
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal

			// other methods
			arma::Mat<fltp> extract_field(arma::Mat<std::complex<fltp> > mLp) const;
			void set_M(const arma::uword num_targets, const arma::Mat<std::complex<fltp> > &M, const int num_exp);
			arma::Mat<std::complex<fltp> > get_M() const;
			
			// enabling parallel calculations
			// void set_use_parallel(const bool use_parallel);
	};


	// localpole to target for magnetic field
	// calculates gradient of scalar potential
	class StMat_Lp2Ta_Grad{
		// properties
		private:
			// number of expansions
			int num_exp_ = 0; 

			// number of target points
			arma::uword num_targets_; 

			// the matrix
			arma::Mat<std::complex<fltp> > M_;

			// use parallel during computing the matrix
			bool use_parallel_ = true;

			// mini matrix
			// arma::SpMat<std::complex<fltp> > Mconv_;
			arma::Mat<std::complex<fltp> >::fixed<3,3> Mconv_;

		// methods
		public:
			// constructor
			StMat_Lp2Ta_Grad();
			StMat_Lp2Ta_Grad(const int num_exp, const arma::Mat<fltp> &dR);	

			// factory
			static ShStMat_Lp2Ta_GradPr create();

			// setting of number of expansions
			void set_num_exp(const int num_exp);
			void set_num_exp(const arma::uword num_exp);

			// calculation of the matrix based on relative position
			void calc_matrix(const arma::Mat<fltp> &dR);
			arma::Mat<std::complex<fltp> > get_matrix() const;
			arma::Mat<std::complex<fltp> > get_matrix(const arma::uword k) const;
			
			// applying of the matrix
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp) const;
			arma::Mat<fltp> apply(const arma::Mat<std::complex<fltp> > &Lp, const arma::uword first_index, const arma::uword last_index) const;
			static arma::Mat<std::complex<fltp> >::fixed<3,3> get_mini_multipole_matrix();

			// applying of the matrix
			bool is_empty() const;
			
			// display methods
			void display() const; // display matrix values in terminal
			void display(const int k) const; // display matrix values in terminal

			// other methods
			arma::Mat<fltp> extract_field(arma::Mat<std::complex<fltp> > mLp) const;
			void set_M(const arma::uword num_targets, const arma::Mat<std::complex<fltp> > &M, const int num_exp);
			arma::Mat<std::complex<fltp> > get_M() const;
			
			// enabling parallel calculations
			void set_use_parallel(const bool use_parallel);
	};

}}

#endif