// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MULTI_SOURCES_HH
#define FMM_MULTI_SOURCES_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>
#include <list>

// mlfmm headers
#include "settings.hh"
#include "targets.hh"
#include "sources.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MultiSources> ShMultiSourcesPr;
	typedef arma::field<ShMultiSourcesPr> ShMultiSourcesPrList;

	// This object combines sources such that the
	// MLFMM can treat them as one object
	// this comes at the cost of overhead
	// but at least it is optional
	class MultiSources: virtual public Sources{
		// properties
		private:
			// list of sources
			ShSourcesPrList srcs_;

			// number of dimensions
			arma::uword num_dim_;

			// number of sources in each object
			arma::uword total_num_sources_;
			arma::Row<arma::uword> num_sources_;

			// keep track of location and where sources are stored
			arma::Row<arma::uword> original_index_;
			arma::Row<arma::uword> original_source_;

			// source index
			arma::Row<arma::uword> source_index_;

			// parallel sorting
			bool use_parallel_ = true;

			// combine sources if possible
			bool combine_ = true;

		// methods
		public:
			// constructor
			MultiSources();
			explicit MultiSources(const ShSourcesPrList &sources); 

			// factory
			static ShMultiSourcesPr create();
			static ShMultiSourcesPr create(const ShSourcesPrList &sources);
			
			// try to combine sources to reduce overhead
			void set_combine(const bool combine = true);

			// factory with source list input
			template<typename T> static ShMultiSourcesPr create(const arma::field<std::shared_ptr<T> > &src_list);

			// add sources
			void add_sources(const ShSourcesPr &src);
			template<typename T> void add_sources(const arma::field<std::shared_ptr<T> > &src_list);
			const ShSourcesPrList& get_sources()const;

			// get the number of source objects
			arma::uword get_num_source_objects() const;

			// setup function
			void setup_multi_sources();

			// setup function
			virtual void setup_sources() override;

			// function for combining sources
			void combine_sources();

			// all source types should have these methods
			// in order to commmunicate with MLFMM
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// get element size
			fltp element_size() const override;

			// sort sources
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// access sorting index
			arma::Row<arma::uword> get_original_index()const;
			arma::Row<arma::uword> get_original_source()const;

			// field calculation from specific sources
			void calc_direct(const ShTargetsPr &tar, const ShSettingsPr &stngs) const override;
			void source_to_target(const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const override; 

			// source to target core
			void source_to_target_core(const arma::uword source_index, const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const;

			// source to multipole step
			void setup_source_to_multipole(const arma::Mat<fltp> &dR, const ShSettingsPr &stngs) override;
			void source_to_multipole(arma::Mat<std::complex<fltp> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const ShSettingsPr &stngs) const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// getting basic information
			arma::uword num_sources() const override;
	};

	// factory with source list input
	template<typename T> ShMultiSourcesPr MultiSources::create(
		const arma::field<std::shared_ptr<T> > &src_list){
		ShMultiSourcesPr multi = MultiSources::create();
		multi->add_sources(src_list);			
		return multi;
	}

	// add a list of sources
	template<typename T> void MultiSources::add_sources(
		const arma::field<std::shared_ptr<T> > &src_list){
		for(arma::uword i=0;i<src_list.n_elem;i++)add_sources(src_list(i));
	}


}}

#endif