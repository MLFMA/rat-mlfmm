// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_CURRENT_SURFACE_HH
#define FMM_CURRENT_SURFACE_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "settings.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class CurrentSurface> ShCurrentSurfacePr;
	typedef arma::field<ShCurrentSurfacePr> ShCurrentSurfacePrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class
	class CurrentSurface: virtual public Sources{
		// properties
		protected:
			// number of dimensions
			arma::uword num_dim_ = 3;

			// number of radii at which elements 
			// are no longer considered point sources
			fltp num_dist_ = 3;

			// number of gauss points used for volume approximation
			arma::sword num_gauss_ = 5;

			// node locations
			arma::Mat<fltp> Rn_;
			
			// surface current density vector at elements in [A/m]
			arma::Mat<fltp> Je_;

			// element definition
			arma::Mat<arma::uword> n_;

			// number of nodes and number of elements
			arma::uword num_nodes_;
			arma::uword num_elements_;

			// calculated element data
			arma::Mat<fltp> Re_; // element centroids
			arma::Row<fltp> element_radius_;

			// calculated element area or volume 
			// in [m^2] or [m^3] respectively
			arma::Row<fltp> Ae_;

			// element face normals (pointing outward)
			arma::Mat<fltp> Ne_;

			// source to multipole matrices
			StMat_So2Mp_J M_J_;
			arma::Mat<fltp> dR_;

		// methods
		public:
			// constructors
			CurrentSurface();
			explicit CurrentSurface(const ShCurrentSurfacePrList &meshes);

			// factory
			static ShCurrentSurfacePr create();
			static ShCurrentSurfacePr create(const ShCurrentSurfacePrList &meshes);

			// setting a hexahedronal mesh with volume elements
			void set_mesh(const ShCurrentSurfacePrList &meshes);
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const arma::Mat<fltp> &Je);

			// calculation
			void calculate_element_areas();

			// set current using nodal magnetisation
			void set_magnetisation_nodes(const arma::Mat<fltp> &Mn);

			// set number of gauss points
			void set_num_gauss(const arma::sword num_gauss);

			// get calculated volume
			arma::Row<fltp> get_area() const;

			// get counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;

			// getting node coordinates
			const arma::Mat<fltp>& get_node_coords() const;

			// getting of elements
			const arma::Mat<arma::uword>& get_elements() const;

			// extract all edges
			arma::Mat<arma::uword> get_edges() const;
			
			// get number of dimensions
			arma::uword get_num_dim() const override;

			// getting number of sources
			arma::uword num_sources() const override;

			// get element size
			fltp element_size() const override;

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// source to multipole
			void setup_source_to_multipole(const arma::Mat<fltp> &dR, const ShSettingsPr &stngs) override;
			void source_to_multipole(arma::Mat<std::complex<fltp> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const ShSettingsPr &stngs) const override; 
			
			// direct field calculation for both A and B
			void calc_direct(const ShTargetsPr &tar, const ShSettingsPr &stngs) const override;
			void source_to_target(const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const override;

			// set elements for solenoid
			void setup_solenoid(const fltp Rin, const fltp Rout, const fltp height, const arma::uword nr, const arma::uword nz, const arma::uword nl, const fltp J);

			// several build-in basic shapes used for testing the code
			void setup_cylinder_shell(const fltp R, const fltp height, const arma::uword nz, const arma::uword nl, const fltp J);
	};

}}

#endif
