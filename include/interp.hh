// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_INTERP_HH
#define FMM_INTERP_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"
#include "rat/common/elements.hh"

// mlfmm headers
#include "sources.hh"
#include "savart.hh"
#include "settings.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Interp> ShInterpPr;
	typedef arma::field<ShInterpPr> ShInterpPrList;

	// hexahedron mesh with volume elements
	// is derived from the sources class (still partially virtual)
	class Interp: virtual public Sources{
		// properties
		protected:
			// type
			char type_;

			// mesh nodes
			arma::Mat<fltp> Rn_;

			// mesh elements
			arma::Mat<arma::uword> n_;

			// element centroids
			arma::Mat<fltp> Re_;
			arma::Row<fltp> element_radius_;

			// list
			arma::Mat<fltp> M_;

			// distance limit
			fltp num_dist_ = 1.01;
			fltp tol_ = 1e-4;

		// methods
		public:
			// constructors
			Interp();
			explicit Interp(const fmm::ShInterpPrList &ip);

			// factory
			static ShInterpPr create();
			static ShInterpPr create(const fmm::ShInterpPrList &ip);

			// set the mesh
			void set_mesh(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
			void set_mesh(const fmm::ShInterpPrList &ip);
			void set_interpolation_values(const char type, const arma::Mat<fltp> &M);

			void setup();

			// get coordinates of the nodes
			arma::Mat<fltp> get_node_coords() const;
			arma::Mat<arma::uword> get_elements() const;

			// all source types should have these methods
			// in order to commmunicate with MLFMM
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// field calculation from specific sources
			void calc_direct(const ShTargetsPr &tar, const ShSettingsPr &stngs) const override;
			void source_to_target(const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const override; 

			// sort sources
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// source to multipole step
			void setup_source_to_multipole(const arma::Mat<fltp> &/*dR*/, const ShSettingsPr &/*stngs*/) override{};
			void source_to_multipole(arma::Mat<std::complex<fltp> > &/*Mp*/, const arma::Row<arma::uword> &/*first_source*/, const arma::Row<arma::uword> &/*last_source*/, const ShSettingsPr &/*stngs*/) const override{};

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// getting basic information
			arma::uword num_sources() const override;

			// setup a cylinder mesh
			void setup_cylinder(const fltp Rin, const fltp Rout, const fltp height, const arma::uword nr, const arma::uword nz, const arma::uword nl);
	};

}}

#endif
