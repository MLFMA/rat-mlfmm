// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_TARGET_POINTS_HH
#define FMM_TARGET_POINTS_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <algorithm>
#include <memory>
#include <unordered_map>

#include <mutex>
#include <atomic>

// common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// mlfmm headers
#include "stmat.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class TargetPoints> ShTargetPointsPr;
	typedef arma::field<ShTargetPointsPr> ShTargetPointsPrList;

	// target points for mlfmm calculation
	// base class partially virtual
	class TargetPoints: virtual public Targets{
		// properties
		protected:
			// store field types
			std::unordered_map<char,arma::uword> field_type_; // field type and number of dimensions

			// field storage
			std::unordered_map<char,arma::Mat<fltp> > M_;

			// coordinates
			arma::Mat<fltp> Rt_;

			// number of target points
			arma::uword num_targets_;

			// multithread lock for adding on M
			std::unique_ptr<std::mutex> lock_ = NULL;

		// methods
		public: 	
			// constructor
			TargetPoints();
			explicit TargetPoints(const arma::Mat<fltp> &Rt);
			
			// factory method
			static ShTargetPointsPr create();
			static ShTargetPointsPr create(const arma::Mat<fltp> &Rt);

			// destructor
			virtual ~TargetPoints(){};

			// setting and adding field types
			void clear_field_type();
			void set_field_type(const char field_type, const arma::uword num_dim);
			void add_field_type(const char field_type, const arma::uword num_dim);
			void set_field_type(const std::string &field_type, const arma::Row<arma::uword> &num_dim);
			void set_field_type(const std::unordered_map<char, arma::uword>&field_type);

			// assertion functions
			bool has_nan() const;
			bool has_field() const;

			// get number of dimensions of specific type
			arma::uword get_target_num_dim(const char field_type) const;

			// setting of coordinates
			void set_target_coords(const arma::Mat<fltp> &Rt);

			// setup coordinates for plane
			void set_xy_plane(
				const fltp ellx, const fltp elly, 
				const fltp xoff, const fltp yoff, 
				const fltp zoff, const arma::uword nx, 
				const arma::uword ny);
			void set_xz_plane(
				const fltp ellx, const fltp ellz, 
				const fltp xoff, const fltp yoff, 
				const fltp zoff, const arma::uword nx, 
				const arma::uword nz);
			
			// allocation of storage matrix
			void allocate() override;
			
			// getting the coordinates
			arma::Mat<fltp> get_target_coords() const override;
			// arma::Mat<fltp> get_target_coords(const char type) const override;
			arma::Mat<fltp> get_target_coords(const arma::uword ft, const arma::uword lt) const override;
			// arma::Mat<fltp> get_target_coords(const char type, const arma::uword ft, const arma::uword lt) const override;
			// arma::Mat<fltp> get_target_coords(const arma::Row<arma::uword> &indices) const;
			
			// getting the number of target points
			arma::uword num_targets() const override;

			// setting of calculated field
			void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd, 
				const bool with_lock = true) override;
			void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd,
				const arma::Row<arma::uword> &ft, 
				const arma::Row<arma::uword> &lt, 
				const arma::Row<arma::uword> &ti, 
				const bool with_lock) override;

			// sorting function
			void sort_targets(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_targets(const arma::Row<arma::uword> &sort_idx) override;

			// getting basic information
			virtual bool has(const char type) const override;
			
			// get field function
			virtual arma::Mat<fltp> get_field(const char type) const override;

			// localpole to target virtual functions
			virtual void setup_localpole_to_target(
				const arma::Mat<fltp> &dR, 
				const arma::uword num_dim, 
				const ShSettingsPr &stngs) override;
			virtual void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const arma::uword num_dim, const ShSettingsPr &stngs) override;
	};

}}

#endif

