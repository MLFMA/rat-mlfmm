// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_EXTRA_HH
#define FMM_EXTRA_HH

// general headers
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/parfor.hh"

#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// library of math functions, which are not specifically class related,
	// that are used throughout the code
	class Extra{
		public:
			// mlfmm specific
			static arma::Mat<int> nmlist(const int nmax);
			static int nm2fidx(const int n, const int m);
			static int hfnm2fidx(const int n, const int m);
			static int polesize(const int nmax);
			static arma::uword polesize(const arma::uword nmax);
			static int dbpolesize(const int nmax);
			static int hfpolesize(const int nmax);
			static fltp Anm(const arma::Col<fltp> &facs, const int n, const int m);
			static arma::Col<fltp> factorial(const int nfacs);
			static std::complex<fltp> ipown(const int n);
			static arma::Mat<fltp> cart2sph(const arma::Mat<fltp> &cartcoord);

			// matrix self-multiplication basically M^N where M is a matrix
			static arma::SpMat<std::complex<fltp> > matrix_self_multiplication(const arma::SpMat<std::complex<fltp> > &M, const arma::uword N);
			static arma::Mat<std::complex<fltp> > matrix_self_multiplication(const arma::Mat<std::complex<fltp> > &M, const arma::uword N);

			// self inductance of objects
			static arma::Row<fltp> calc_wire_inductance(const arma::Row<fltp> &d, const arma::Row<fltp> &l, const fltp mu = 1.0);
			static arma::Row<fltp> calc_ribbon_inductance(const arma::Row<fltp> &w, const arma::Row<fltp> &l, const arma::Row<fltp> &t);

			// display function for poles
			static void display_pole(const int num_exp, const arma::Col<std::complex<fltp> > &M, const rat::cmn::ShLogPr &lg, const arma::uword num_digits = 1llu);
			static void display_pole(const int num_exp, const arma::Col<fltp> &M, const rat::cmn::ShLogPr &lg, const arma::uword num_digits = 1llu);
			static void display_pole(const int nmax, const int mmax, const arma::Col<std::complex<fltp> > &M, const rat::cmn::ShLogPr &lg, const arma::uword num_digits = 1llu);
			static void display_pole(const int nmax, const int mmax, const arma::Col<fltp> &M, const rat::cmn::ShLogPr &lg, const arma::uword num_digits = 1llu);

			// convert float
			template<typename T1,typename T2>
			static arma::Mat<T1> parallel_convert(const arma::Mat<T2> &M, const arma::uword chunksize = 10000);
	};

	// template function (must be in header)
	template<typename T1,typename T2>
	arma::Mat<T1> Extra::parallel_convert(const arma::Mat<T2> &M, const arma::uword chunksize){
		const arma::uword num_chunks = M.n_elem/chunksize + 1;
		arma::Mat<T1> Mf(M.n_rows, M.n_cols, arma::fill::none);
		cmn::parfor(0,num_chunks,true,[&](int i, int /*cpu*/){
			const arma::uword idx1 = i*chunksize;
			const arma::uword idx2 = std::min((i+1)*chunksize, M.n_elem);
			for(arma::uword j=idx1;j<idx2;j++){
				Mf(j) = T1(M(j));
			}
		});
		return Mf;
	}

}}

#endif