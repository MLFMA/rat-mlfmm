// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MAGNETIC_CHARGES_HH
#define FMM_MAGNETIC_CHARGES_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <memory>

// common headers
#include "rat/common/error.hh"

// mlfmm headers
#include "sources.hh"
#include "mgntargets.hh"
#include "stmat.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MgnCharges> ShMgnChargesPr;

	// fictitious magnetic charges for field calculation
	class MgnCharges: virtual public Sources, public MgnTargets{
		// properties
		protected:
			// number of dimensions
			const arma::uword num_dim_ = 1llu;

			// coordinates in [m]
			arma::Mat<fltp> Rs_;

			// magnetic charge
			arma::Row<fltp> sigma_s_;

			// softening factor [m]
			arma::Row<fltp> epss_;

			// number of sources
			arma::uword num_sources_ = 0llu;

			// source to multipole matrices
			fmm::StMat_So2Mp_J M_J_;
			arma::Mat<fltp> dRs_;

			// multipole operation matrices
			// fmm::StMat_Lp2Ta M_A_; // for vector potential
			// arma::Mat<fltp> dRl2p_;

		// methods
		public:
			// constructor
			MgnCharges();
			MgnCharges(
				const arma::Mat<fltp> &Rs, 
				const arma::Row<fltp> &sigma_s, 
				const arma::Row<fltp> &epss);

			// factory
			static ShMgnChargesPr create();
			static ShMgnChargesPr create(
				const arma::Mat<fltp> &Rs, 
				const arma::Row<fltp> &sigma_s, 
				const arma::Row<fltp> &epss);

			// set coordinates and currents of elements
			void set_coords(const arma::Mat<fltp> &Rs);
			void set_magnetic_charge(const arma::Row<fltp> &sigma_s);
			void set_softening(const arma::Row<fltp> &eps);

			// sort function
			void sort_sources(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_sources(const arma::Row<arma::uword> &sort_idx) override;

			// getting source coordinates
			arma::Mat<fltp> get_source_coords() const override;
			// arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const override;

			// get charge
			const arma::Mat<fltp>& get_charge() const;

			// count number of sources
			arma::uword num_sources() const override;

			// get number of dimensions
			arma::uword get_num_dim() const override;

			// source to multipole
			void setup_source_to_multipole(
				const arma::Mat<fltp> &dR, 
				const fmm::ShSettingsPr &stngs) override;
			void source_to_multipole(
				arma::Mat<std::complex<fltp> > &Mp, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const fmm::ShSettingsPr &stngs) const override;
			
			// direct field calculation for both A and B
			void calc_direct(
				const fmm::ShTargetsPr &tar, 
				const fmm::ShSettingsPr &stngs) const override;
			void source_to_target(
				const fmm::ShTargetsPr &tar, 
				const arma::Col<arma::uword> &target_list, 
				const arma::field<arma::Col<arma::uword> > &source_list, 
				const arma::Row<arma::uword> &first_source, 
				const arma::Row<arma::uword> &last_source, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const fmm::ShSettingsPr &stngs) const override;

			// calculate force on charges
			arma::Mat<fltp> calc_force()const;
	};

}}

#endif
