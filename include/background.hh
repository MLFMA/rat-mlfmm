// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_BACKGROUND_HH
#define FMM_BACKGROUND_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <cmath>
#include <algorithm>

// specific headers
#include "rat/common/error.hh"
#include "targets.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Background> ShBackgroundPr;

	// background field
	class Background{
		// properties
		protected:
			// the type of background field
			arma::field<char> field_type_;
			arma::field<arma::Col<fltp>::fixed<3> > bgfld_;
			arma::field<arma::Mat<fltp>::fixed<3,3> > bggrad_;

		// methods
		public:
			// constructor
			Background();

			// factory
			static ShBackgroundPr create();

			// check if background has field
			bool has_field()const;

			// helper for setting magnetic
			void set_magnetic(const arma::Col<fltp>::fixed<3> &Hbg);

			// add field
			void add_field(const char type, const arma::Col<fltp>::fixed<3>&bgfld, const arma::Mat<fltp>::fixed<3,3>&bggrad);

			// set field to targets
			void calc_field(const ShTargetsPr &tar) const;
	};

}}

#endif
