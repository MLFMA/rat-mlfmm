// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_MULTI_TARGETS_HH
#define FMM_MULTI_TARGETS_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>

// mlfmm headers
#include "targets.hh"
#include "sources.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class MultiTargets> ShMultiTargetsPr;
	typedef arma::field<ShMultiTargetsPr> ShMultiTargetsPrList;

	// This object combines sources such that the
	// MLFMM can treat them as one object
	// this comes at the cost of overhead
	// but at least it is optional
	class MultiTargets: public Targets{
		// properties
		private:
			// list of targets
			ShTargetsPrList tars_;

			// number of sources in each object
			arma::uword total_num_targets_;
			arma::Row<arma::uword> num_targets_;

			// keep track of location and where sources are stored
			arma::Row<arma::uword> original_index_;
			arma::Row<arma::uword> original_target_;

			// target index
			arma::Row<arma::uword> target_index_;

			// parallel sorting
			bool use_parallel_ = true;

		// methods
		public:
			// constructor
			MultiTargets();
			explicit MultiTargets(const ShTargetsPrList &targets); 

			// factory
			static ShMultiTargetsPr create();
			static ShMultiTargetsPr create(const ShTargetsPrList &targets);
			
			// factory with source list input
			template<typename T> static ShMultiTargetsPr create(const arma::field<std::shared_ptr<T> > &tar_list);

			// setup multi-targets
			void setup_multi_targets();

			// setup function
			void setup_targets() override;

			// post processing
			void post_process() override;

			// add targets
			void add_targets(const ShTargetsPr &tar);
			template<typename T> void add_targets(const arma::field<std::shared_ptr<T> > &tar_list);
			
			// get the number of target objects
			arma::uword get_num_target_objects() const;

			// get field function
			arma::Mat<fltp> get_field(const char type) const override;

			// getting basic information
			bool has(const char type) const override;

			// allocation of storage matrix
			void allocate() override;

			// sorting function
			void sort_targets(const arma::Row<arma::uword> &sort_idx) override;
			void unsort_targets(const arma::Row<arma::uword> &sort_idx) override;

			// access sorting index
			arma::Row<arma::uword> get_original_index()const;
			arma::Row<arma::uword> get_original_target()const;

			// getting the coordinates
			arma::Mat<fltp> get_target_coords() const override;
			// arma::Mat<fltp> get_target_coords(const arma::Row<arma::uword> &indices) const override;
			arma::Mat<fltp> get_target_coords(const arma::uword ft, const arma::uword lt) const override;
			// arma::Mat<fltp> get_target_coords(const arma::Row<arma::uword> &indices) const override;
			// arma::Mat<fltp> get_target_coords(const char type) const override;
			// arma::Mat<fltp> get_target_coords(const char type, const arma::uword ft, const arma::uword lt) const override;

			// getting the number of target points
			arma::uword num_targets() const override;

			// setting of calculated field
			void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd, 
				const bool with_lock = true) override;
			void add_field(
				const char type, 
				const arma::Mat<fltp> &Madd,
				const arma::Row<arma::uword> &ft, 
				const arma::Row<arma::uword> &lt, 
				const arma::Row<arma::uword> &ti, 
				const bool with_lock) override;
		
			// localpole to target functions
			void setup_localpole_to_target(
				const arma::Mat<fltp> &dR, 
				const arma::uword num_dim, 
				const ShSettingsPr &stngs) override;
			void localpole_to_target(
				const arma::Mat<std::complex<fltp> > &Lp, 
				const arma::Row<arma::uword> &first_target, 
				const arma::Row<arma::uword> &last_target, 
				const arma::uword num_dim, 
				const ShSettingsPr &stngs) override;
	};


	// special factory method
	template<typename T> ShMultiTargetsPr MultiTargets::create(
		const arma::field<std::shared_ptr<T> > &tar_list){
		ShMultiTargetsPr multi = MultiTargets::create();
		multi->add_targets(tar_list);
		return multi;
	}

	// add a list of targets
	template<typename T> void MultiTargets::add_targets(
		const arma::field<std::shared_ptr<T> > &tar_list){
		for(arma::uword i=0;i<tar_list.n_elem;i++)add_targets(tar_list(i));
	}

}}

#endif