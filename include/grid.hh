// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_GRID_HH
#define FMM_GRID_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <cmath> // contains M_PI
#include <algorithm>
#include <thread>
#include <memory>

// common headers
#include "rat/common/extra.hh"
#include "rat/common/log.hh"

// mlfmm headers
#include "ilist.hh"
#include "fmmmat.hh"
#include "settings.hh"
#include "stmat.hh"
#include "savart.hh"
#include "sources.hh"
#include "targets.hh"


// Grid is responsible for assigning morton indices to elements

// levels are defined as
// level 0 - morton index 0
// level 1 - morton index 0-7
// level 2 - morton index 0-63
// level 3 - morton index 0-512
// ....
// level n - morton index 0-8^n

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Grid> ShGridPr;

	// harmonics class template
	class Grid{
		// properties
		protected:
			// interaction lists
			ShIListPr ilist_ = NULL;
			ShSettingsPr stngs_ = NULL;

			// targets and sources
			ShTargetsPr tar_ = NULL;
			ShSourcesPr src_ = NULL;

			// counters
			arma::uword num_sources_ = 0;
			arma::uword num_targets_ = 0;
			arma::uword num_elements_ = 0;

			// grid properties
			arma::uword num_levels_ = 0;
			fltp grid_size_ = 0;
			arma::Col<fltp>::fixed<3> grid_position_ = {0,0,0};

			// tracking of elements
			arma::Row<arma::uword> morton_index_;
			arma::Row<arma::uword> sort_index_;
			// arma::Row<arma::uword> original_index_;
			arma::Row<arma::sword> element_type_; // positive are sources and negative are targets

			// sorting indexes
			arma::Row<arma::uword> source_sort_index_;
			arma::Row<arma::uword> target_sort_index_;

			// tracking of source and target blocks
			arma::uword num_source_nodes_;
			arma::uword num_target_nodes_;
			arma::Row<arma::uword> first_source_;
			arma::Row<arma::uword> last_source_;
			arma::Row<arma::uword> first_target_;
			arma::Row<arma::uword> last_target_;

			// position of the mlfmm nodes
			arma::Mat<fltp> Rn_;
			arma::Mat<fltp> dR_;

			// number of rescales performed in last setup
			arma::uword num_rescale_ = 0;

			// average number of sources in box
			fltp mean_num_sources_;
			fltp mean_num_targets_;

			// translation matrices
			ShFmmMat_Mp2MpPr multipole_to_multipole_matrix_ = NULL;
			ShFmmMat_Lp2LpPr localpole_to_localpole_matrix_ = NULL;

			// transformation matrices
			ShFmmMat_Mp2LpPr multipole_to_localpole_matrix_ = NULL;

			// timing for the last calculation
			fltp last_setup_time_ = 0;
			fltp last_matrix_time_ = 0;

			// timing for the lifetime of this object
			fltp life_setup_time_ = 0;
			fltp life_matrix_time_ = 0; 

		// methods
		public:
			// constructor
			Grid();
			Grid(const ShSettingsPr &stngs, const ShIListPr &ilist, const ShSourcesPr &src, const ShTargetsPr &tar);

			// factory
			static ShGridPr create();
			static ShGridPr create(const ShSettingsPr &stngs, const ShIListPr &ilist, const ShSourcesPr &src, const ShTargetsPr &tar);

			// virtual destructor
			virtual ~Grid(){};

			// settings object
			void set_settings(const ShSettingsPr &stngs);

			// set interaction list
			void set_ilist(const ShIListPr &ilist);

			// setting sources
			void set_sources(const ShSourcesPr &src);
			
			// setting targets
			void set_targets(const ShTargetsPr &tar);

			// getting settings
			arma::Col<fltp>::fixed<3> center_position() const;
			arma::Col<fltp>::fixed<3> get_position() const;
			arma::uword get_num_levels() const;
			arma::uword get_num_exp() const;

			// size of the grid
			fltp get_box_size() const; // box size at maximum level
			fltp get_box_size(const arma::uword ilevel) const; // box size at specified level
			fltp get_grid_size() const;
			
			// info on nodes
			arma::Mat<fltp> get_node_locations(const arma::Mat<arma::uword> &indices) const;
			arma::Mat<arma::uword> const& get_morton() const;
			arma::Mat<arma::uword> get_is_source() const;
			arma::Mat<arma::uword> get_is_target() const;

			// get first and last source lists
			const arma::Row<arma::uword>& get_first_source()const;
			const arma::Row<arma::uword>& get_last_source()const;
			const arma::Row<arma::uword>& get_first_target()const;
			const arma::Row<arma::uword>& get_last_target()const;

			// get source and target sorting indexes
			const arma::Row<arma::uword>& get_source_sort_index()const;
			const arma::Row<arma::uword>& get_target_sort_index()const;

			// get sorting array
			// arma::Row<arma::uword> get_original_index() const;
			const arma::Row<arma::uword>& get_sort_index() const;

			// method for setting up the grid itself
			virtual void setup(const cmn::ShLogPr &lg = rat::cmn::NullLog::create()); 
			void setup_indices();

			// these could go in separate class
			// methods for setting MLFMM matrices
			void setup_matrices();
			void setup_translation_matrices();
			void setup_transformation_matrices();
			
			// number of sources, targets and elements
			arma::uword get_num_sources() const;
			arma::uword get_num_targets() const;
			arma::uword get_num_elements() const;

			// other information
			fltp get_mean_num_sources() const;
			fltp get_mean_num_targets() const;
			arma::uword get_num_rescale() const;

			// morton index conversion functions (for testing)
			static arma::Mat<arma::uword> morton2grid(const arma::Mat<arma::uword> &morton_index);
			static arma::Mat<arma::uword> grid2morton(const arma::Mat<arma::uword> &grid_index, const arma::uword level);

			// multipole method steps
			virtual void source_to_target(const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list);
			void source_to_multipole(arma::Mat<std::complex<fltp> > &Mp) const;
			virtual void localpole_to_target(const arma::Mat<std::complex<fltp> > &Lp);

			// methods for accessing the multipole matrices
			const ShFmmMat_Mp2MpPr& get_multipole_to_multipole_matrix();
			const ShFmmMat_Lp2LpPr& get_localpole_to_localpole_matrix();
			const ShFmmMat_Mp2LpPr& get_multipole_to_localpole_matrix();

			// mlfmm pre and post methods
			void fmm_setup();
			void fmm_post();
			void sort_srctar();
			void unsort_srctar();

			// number of dimensions
			arma::uword get_num_dim() const;

			// display general information about this grid
			void display(const cmn::ShLogPr &lg) const;
	};

}}

#endif