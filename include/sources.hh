// Copyright 2022 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef FMM_SOURCES_HH
#define FMM_SOURCES_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cassert>
#include <memory>

// mlfmm headers
#include "targets.hh"
#include "settings.hh"

// code specific to Rat
namespace rat{namespace fmm{

	// shared pointer definition
	typedef std::shared_ptr<class Sources> ShSourcesPr;
	typedef arma::field<ShSourcesPr> ShSourcesPrList;

	// source points for mlfmm calculation
	// base class (mostly virtual)
	class Sources{
		// virtual methods (different for all sources)
		public:
			// constructor
			Sources();

			// virtual destructor (obligatory)
			virtual ~Sources(){};
			
			// all source types should have these methods
			// in order to commmunicate with MLFMM
			virtual arma::Mat<fltp> get_source_coords() const;
			// virtual arma::Mat<fltp> get_source_coords(const arma::Row<arma::uword> &indices) const;

			// field calculation from specific sources
			virtual void calc_direct(const ShTargetsPr &tar, const ShSettingsPr &stngs) const;
			virtual void source_to_target(const ShTargetsPr &tar, const arma::Col<arma::uword> &target_list, const arma::field<arma::Col<arma::uword> > &source_list, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const arma::Row<arma::uword> &first_target, const arma::Row<arma::uword> &last_target, const ShSettingsPr &stngs) const; 

			// sort sources
			virtual void sort_sources(const arma::Row<arma::uword> &sort_idx);
			virtual void unsort_sources(const arma::Row<arma::uword> &sort_idx);

			// source to multipole step
			virtual void setup_source_to_multipole(const arma::Mat<fltp> &dR, const ShSettingsPr &stngs);
			virtual void source_to_multipole(arma::Mat<std::complex<fltp> > &Mp, const arma::Row<arma::uword> &first_source, const arma::Row<arma::uword> &last_source, const ShSettingsPr &stngs) const;

			// get number of dimensions
			virtual arma::uword get_num_dim()const;

			// setup sources
			virtual void setup_sources();

			// getting basic information
			virtual arma::uword num_sources() const;

			// get typical element size (to limit grid)
			virtual fltp element_size() const;
	};

}}

#endif